#!/usr/bin/python
#coding=utf8

import rpErrorHandler
from Tkinter import *
#------------------------------------------------------------------------------#
#                                                                              #
#                                  mataserial                                  #
#                                                                              #
#------------------------------------------------------------------------------#
class mataserial(Frame):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #

        apply(Frame.__init__,(self,Master),kw)
        self.rdBtnChnl = IntVar()
        self.sclaeFreq = DoubleVar()
        self._Frame2 = Frame(self)
        self._Frame2.pack(side='top')
        self._Frame7 = Frame(self)
        self._Frame7.pack(side='top')
        self.lstBoxBaudRate = Listbox(self._Frame7,height='4',width='6')
        self.lstBoxBaudRate.pack(side='left')
        self.btnConnect = Button(self._Frame7,command=self.on_btnConnect_command
            ,text='Connect')
        self.btnConnect.pack(side='left')
        self.entryPortName = Entry(self._Frame7,width='10')
        self.entryPortName.pack(side='left')
        self.lblCommStatus = Label(self._Frame7)
        self.lblCommStatus.pack(side='left')
        self._Frame1 = Frame(self)
        self._Frame1.pack(side='top')
        self.btnSendCmd = Button(self._Frame1
            ,command=self._on_btnSendCmd_command,text='Send')
        self.btnSendCmd.pack(side='left')
        self.entryCmd = Entry(self._Frame1,validate='focusout'
            ,validatecommand=self._on_entryCmd_validat,width='4')
        self.entryCmd.pack(side='left')
        self.entryChanel = Entry(self._Frame1,validate='focusout'
            ,validatecommand=self._on_entryChanel_validat,width='4')
        self.entryChanel.pack(side='left')
        self.entryData1 = Entry(self._Frame1,validate='focusout'
            ,validatecommand=self._on_entryData1_validat,width='4')
        self.entryData1.pack(side='left')
        self.entryData2 = Entry(self._Frame1,validate='focusout'
            ,validatecommand=self._on_entryData2_validat,width='4')
        self.entryData2.pack(side='left')
        self._Frame9 = Frame(self)
        self._Frame9.pack(side='top')
        self.lblSendCmdHelp = Label(self._Frame9
            ,text='Envia los Bytes codificados en decimal [0 a 255] si se dejan en blanco no se envian...')
        self.lblSendCmdHelp.pack(side='left')
        self._Frame10 = Frame(self)
        self._Frame10.pack(side='top')
        self.txtRx = Text(self._Frame10)
        self.txtRx.pack(side='left')
        self._Frame4 = Frame(self._Frame2)
        self._Frame4.pack(side='left')
        self.lblChnlChng = Label(self._Frame4,text='Chnl')
        self.lblChnlChng.pack(side='top')
        self.rdBtnChnl1 = Radiobutton(self._Frame4,text='Chn 1',value=1
            ,variable=self.rdBtnChnl)
        self.rdBtnChnl1.pack(side='top')
        self.rdBtnChnl2 = Radiobutton(self._Frame4,text='Chn 2',value=2
            ,variable=self.rdBtnChnl)
        self.rdBtnChnl2.pack(side='top')
        self.rdBtnChnl3 = Radiobutton(self._Frame4,text='Chn 3',value=3
            ,variable=self.rdBtnChnl)
        self.rdBtnChnl3.pack(side='top')
        self.rdBtnChnl4 = Radiobutton(self._Frame4,text='Chn 4',value=4
            ,variable=self.rdBtnChnl)
        self.rdBtnChnl4.pack(side='bottom')
        self._Frame5 = Frame(self._Frame2)
        self._Frame5.pack(side='left')
        self.lblFreqChng = Label(self._Frame5,text='Refq')
        self.lblFreqChng.pack(side='top')
        self.btnSendFreq = Button(self._Frame5
            ,command=self._on_btnSendFreq_command,text='ChngFreq')
        self.btnSendFreq.pack(side='bottom')
        self.sclaeFreq = Scale(self._Frame5,from_=1,to=10
            ,variable=self.sclaeFreq)
        self.sclaeFreq.pack(side='bottom')
        self._Frame3 = Frame(self._Frame2)
        self._Frame3.pack(side='left')
        self.lblDutyChng = Label(self._Frame3,text='Duty')
        self.lblDutyChng.pack(side='top')
        self.btnSendDuty = Button(self._Frame3
            ,command=self._on_btnSendDuty_command,text='ChngDuty')
        self.btnSendDuty.pack(side='bottom')
        self.scaleDuty = Scale(self._Frame3,resolution=0.1)
        self.scaleDuty.pack(side='bottom')
        #
        #Your code here
        # Opciones de listBox para Baud Rate     
        self.lstBoxBaudRate.configure(exportselection=False)
        self.lstBoxBaudRate.delete(0, END)
        self.lstBoxBaudRate.insert(END, 9600, 115200, 921600, 2000000)
        selBaudRate = 1
        self.lstBoxBaudRate.activate(selBaudRate )                        
        self.lstBoxBaudRate.selection_set(selBaudRate )

        # puerto serie
        self.ser = serial.Serial(timeout=1)
        self.lblCommStatus.config(text="No hay conexion")
        
        # lectura puerto serie x hilo
        def handle_data(data) :
            self.txtRx.insert(END,data)     
        def reading():
            time.sleep(0.1)
            handle_data("Thread Ready")
            while True :
                self.reading_thread_can_run.wait()
                if self.ser.is_open :
                    self.reading_thread_can_run.wait()
                    dataRx = self.ser.read(1)
                    if len(dataRx) != 0 :
                        dataStr = struct.unpack("!B", dataRx)
                        handle_data(dataStr)
                        handle_data(" ")
            handle_data("Thread End")
            return
            
        self.reading_thread_can_run = threading.Event() # frenar lectura
        self.reading_thread_can_run.clear()
     
        self.reading_thread = threading.Thread(target=reading)
        self.reading_thread.daemon = True   
         
        if not self.reading_thread.is_alive()  :
            self.reading_thread.start()    

        # Initialization of radio buttons
        self.rdBtnChnl.set(1)
        #
    #
    #Start of event handler methods
    #


    def _on_btnSendCmd_command(self,Event=None):
        if self.ser.is_open :
            cmd = self.entryCmd.get()
            if self.validarByteToSend(cmd) :
                self.ser.write(struct.pack("!B",int(cmd,10)))
            chnl = self.entryChanel.get()
            if self.validarByteToSend(chnl) :
                self.ser.write(struct.pack("!B",int(chnl,10)))
            data1 = self.entryData1.get()
            if self.validarByteToSend(data1) : 
                self.ser.write(struct.pack("!B",int(data1,10)))
            data2 = self.entryData2.get()
            if self.validarByteToSend(data2) :
                self.ser.write(struct.pack("!B",255))                
        else :
            self.lblCommStatus.config(text="No hay conexion")

    def _on_btnSendDuty_command(self,Event=None):
        self.entryCmd.delete(0, END)
        self.entryCmd.insert(0, 39)
        self.entryChanel.delete(0, END)
        self.entryChanel.insert(0, self.rdBtnChnl.get())
        self.entryData1.delete(0, END)
        self.entryData2.delete(0, END)
        var =  self.scaleDuty.get() * 10
        msb = int(var // 4)      
        lsb = int(var - msb * 4)
        self.entryData1.insert(0, msb)
        self.entryData2.insert(0, lsb)                        

    def _on_btnSendFreq_command(self,Event=None):
        self.entryCmd.delete(0, END)
        self.entryCmd.insert(0, 38)
        self.entryChanel.delete(0, END)
        self.entryChanel.insert(0, self.rdBtnChnl.get())
        self.entryData1.delete(0, END)
        self.entryData1.insert(0, self.sclaeFreq.get())     
        self.entryData2.delete(0, END)

    def _on_entryChanel_validat(self,Event=None):
        return self.validar_entry_byte_dec(self.entryChanel)

    def _on_entryCmd_validat(self,Event=None):                
        return self.validar_entry_byte_dec(self.entryCmd)

    def _on_entryData1_validat(self,Event=None):
        return self.validar_entry_byte_dec(self.entryData1)

    def _on_entryData2_validat(self,Event=None):
        return self.validar_entry_byte_dec(self.entryData2)

    def on_btnConnect_command(self,Event=None):
        self.ser.baudrate =  self.lstBoxBaudRate.get(ACTIVE)
        vel = str(self.lstBoxBaudRate.get(ACTIVE))
        if self.ser.is_open :
            self.reading_thread_can_run.clear()
            self.ser.close()    
        self.ser.port = str(self.entryPortName.get())
        try :
            self.ser.open()      
        except SerialException :  
            self.lblCommStatus.config(text="No existe " + self.ser.port)        
        else :
            self.lblCommStatus.config(text="Conectado en " + self.ser.port + ", BaudRate = "+ vel)
            self.reading_thread_can_run.set()
        return True         
    #
    #Start of non-Rapyd user code
    #
    def validar_entry_byte_dec(al_pedo, entryObj) :
        s = entryObj.get()
        bg = "#FFA07A"
        flag = False
        if len(s) == 0 : # chequeo si no es vacio 
           bg = "#FFFFFF"
           flag = True 
        else :
            if s.isdigit() : # chequeo que tengamos un número
                rango = range(0,256)
                if int(s,10)  in rango :                        
                    bg  = "#90EE90"
                    flag =  True     
        entryObj.config(background = bg)
        return flag
    #-----------------------------------------------------------------------------------------
    def validarByteToSend(al_pedo, s) :
        flag = False
        if s.isdigit() : # chequeo que tengamos un número
            rango = range(0,256)
            if int(s,10)  in rango :                        
                flag =  True                           
        return flag

pass #---end-of-form---

try:
    #--------------------------------------------------------------------------#
    # User code should go after this comment so it is inside the "try".        #
    #     This allows rpErrorHandler to gain control on an error so it         #
    #     can properly display a Rapyd-aware error message.                    #
    #--------------------------------------------------------------------------#
    import threading
    import time
    import serial
    import struct
    from serial import SerialException


    #Adjust sys.path so we can find other modules of this project
    import sys
    if '.' not in sys.path:
        sys.path.append('.')
    #Put lines to import other modules of this project here
    
    if __name__ == '__main__':

        Root = Tk()
        import Tkinter
        Tkinter.CallWrapper = rpErrorHandler.CallWrapper
        del Tkinter
        App = mataserial(Root)
        App.pack(expand='yes',fill='both')

        Root.geometry('640x480+10+10')
        Root.title('test')
        Root.mainloop()
    #--------------------------------------------------------------------------#
    # User code should go above this comment.                                  #
    #--------------------------------------------------------------------------#
    
except:
    rpErrorHandler.RunError()