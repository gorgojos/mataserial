# Mata Serial
App python 2 para enviar comandos via serie. 

Mata Serial es Software Libre segun los terminos de la [licencia GPL v3](https://www.gnu.org/copyleft/gpl.html)
## Dependencias para uso

[python 2.7](https://www.python.org/download/releases/2.7/)

[pySerial](https://github.com/pyserial/pyserial#installation)

## Como descargar y lanzar la app
Clonar o [Bajar repositorio](https://gitlab.com/gorgojos/mataserial/-/archive/master/mataserial-master.zip) 
y ejecutar python mataserie.py

## Desarrollada con [Rapyd-Tk](http://www.bitflipper.ca/rapyd/)

# Aportes bien venidos!